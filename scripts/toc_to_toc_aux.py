#SSW2014: A description of what this does (needed for the bib files)
#The main purpose of this script is to print out all the content contributions to the proceedings including the start/end pages.
#THis is done by processing the proceedings.toc file.

lines = open("Proceedings.toc").readlines()

#SSW2014: set the year
year = "2016"

import subprocess
# Number of pages in the header is hard-coded
frontmattersize = 9
num_pages = [int(x.split()[1]) for x in subprocess.Popen(["pdfinfo", "Proceedings.pdf"], stdout=subprocess.PIPE).stdout.readlines() if x.startswith('Pages:')][0] - frontmattersize

#print "TRACE num_pages",num_pages

#KMV above code didn't work for me, hard-coding length (153 pages - 8 pages header)
#SSW2014: Above works for me... not hardcoding.
#num_pages=145


# Stopped using hyperef package and had to do this to make the scripts work again...
lines = [x.strip() + "{section*.7}" for x in lines]
content_lines = []
for i,line in enumerate(lines):

    # Don't want the non-paper things to be included here
    if '\contentsline' not in line:
		#print "TRACE excluding",line
		continue
    else:
		#print "TRACE including",line
		content_lines.append(line)

# SSW2014: Just iterate through content lines.
#   Needed a second array here because sometimes "next_line" wasn't a content line (and so you can't get the end page numbners).
i=0
for line in content_lines:
    line = line.decode('utf8')

    #print "TRACE clean_line",line

    # fix authors
    line = line.split("}")
    line[2] = line[2].replace(',', ' and')
    line = "}".join(line)

    # fix last page
    if i == len(content_lines) - 1:
        last_page = num_pages
    else:
		next_line = content_lines[i+1]
		last_page = int(next_line.split('}')[-3].lstrip('{')) - 1

    line = line.replace("section*.7", str(last_page))

    #Only print lines corresponding to content entries (ie. requiring a bib file)
    if '\\it' in line :  #ignore section division pages, eg. "Invited talks", "Long papers" etc.
        print line.encode('utf')

    #increment i
    i += 1

# lines = [x for x in lines if '\\it' in x]
