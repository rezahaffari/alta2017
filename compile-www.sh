python ./scripts/schedule_web.py < programme.tsv > ../../alta2016-website/programme.html
cut -f 2,3 shortpapers.txt | sed -e 's/^/<tr><th><\/th><td>/; s/	/<\/td><td><i>/; s/$/<\/i><\/td><\/tr>/' > ../../alta2016-website/programme-short.html
cut -f 2,3 sharedtaskpapers.txt | sed -e 's/^/<tr><th><\/th><td>/; s/	/<\/td><td><i>/; s/$/<\/i><\/td><\/tr>/' > ../../alta2016-website/programme-shared.html
