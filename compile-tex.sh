#!/bin/bash

# count pages in the pdfs

./scripts/pages.sh > page_counts.txt

# augment paper lists with page counts

./scripts/prepend_page_counts.sh longpapers.txt{,.pc}
./scripts/prepend_page_counts.sh shortpapers.txt{,.pc}
./scripts/prepend_page_counts.sh sharedtaskpapers.txt{,.pc}

# create the latex files for the proceedings
mkdir -p tex

# 1) main body of the proceedings

perl scripts/proc.pl longpapers.txt.pc longorder.txt > tex/longpapers.tex
perl scripts/proc.pl shortpapers.txt.pc shortorder.txt > tex/shortpapers.tex
perl scripts/proc.pl sharedtaskpapers.txt.pc sharedtaskorder.txt > tex/sharedtaskpapers.tex

# 2) schedule

python scripts/tsv_to_latex.py Tuesday "6 December 2016" > tex/schedule-tuesday.tex
python scripts/tsv_to_latex.py Wednesday "7 December 2016" > tex/schedule-wednesday.tex

# detail of the short papers and shared task papers (part of schedule)

cut -f 2,3 shortpapers.txt | sed 's/	/\\\\ \& \\emph{/g; s/$/} \\\\/; s/^/\& /' > tex/shortpapers-tlist.tex 
cut -f 2,3 sharedtaskpapers.txt | tail -n +2 | sed 's/	/\\\\ \& \\emph{/g; s/$/} \\\\/; s/^/\& /' > tex/sharedtask-tlist.tex

# compile proceedings

pdflatex Proceedings.tex
pdflatex Proceedings.tex
pdflatex Proceedings.tex

# make the packaged outputs

mkdir -p anth/ALTA/bib anth/ALTA/pdf 
python scripts/toc_to_toc_aux.py > Proceedings.toc_aux

perl scripts/proc2.pl
perl scripts/proc3.pl

zip -r U16.zip U16/

