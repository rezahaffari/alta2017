#!/usr/bin/perl -w

#This creates all the ACL Anthology files

use strict;

use vars qw (@Pos $i $h @F $a $b $t $au $fm);

#Create bib for book

#the size of the front matter in pages
$fm = 9; 

# ***** Need to update this stuff *****
open(O,">anth/ALTA/bib/ALTA00.bib")||die;
print O "\@Book\{ALTA2016\:2016,\n";
print O  "editor    = \{Trevor Cohn\},\n";
print O "title \= \{Proceedings of the Australasian Language Technology Association Workshop 2016\}\,\n";
print O "month     \= \{December\}\,\n";
print O "year      \= \{2016\}\,\n";
print O "address   \= \{Melbourne\, Australia\}\,\n";
print O "url       = \{http://www.aclweb.org/anthology/U16-1\}\n\}\n";
close(O);

# Just once to separate the pdf into single files
system("mkdir pdf_pages");
chdir("pdf_pages");
system("pdftk ../Proceedings.pdf burst");

chdir("..");
undef @Pos;
open(I,"Proceedings.toc_aux")||die;
$i = 1;
while(<I>) {
    chomp;

    /\\it (.*?) ?\} \\\\ ?(.*?) ?\}\{(\d+)\}\{(\d+)\}$/;
    $t = $1; $au = $2; $a = $3; $b = $4;
    push @Pos,"$a\-$b";
    if ($i < 10) {
	open(O,">anth/ALTA/bib/ALTA0$i.bib")||die;
    }
    else {
	open(O,">anth/ALTA/bib/ALTA$i.bib")||die;
    }
    $_ = $au;
    s / and /:/g;
    s /([^:]*) (.*)[:]?/\1/g;
    s /[ \'\,\"\\]//g;
    print O "\@InProceedings\{$_:2016:ALTA2016\,\n";
    print O "author \= \{$au\}\,\n";
    print O "title \= \{$t\}\,\n";
    print O "booktitle \= \{Proceedings of the Australasian Language Technology Association Workshop 2016\}\,\n";
    print O "month     \= \{December\}\,\n";
    print O "year      \= \{2016\}\,\n";
    print O "address   \= \{Melbourne\, Australia\}\,\n";
    # PC 23.11.2012
    # $_ = $a + $b - 1;
    $_ = $b;
    print O "pages \= \{$a\-\-$_\}\,\n";
    $_ = 1000 + $i;
    print O "url \= \{http://www.aclweb.org/anthology/U16\-$_\}\n";
    print O "\}\n";
    close(O);
    $i++;
}
close(I);

# die;

#copy preface
undef @F;
for($h=1;$h<=$fm;$h++) {
    $_ = sprintf("%04d", $h);
    push @F,"pdf_pages/pg\_$_\.pdf";
}
$_ = join " ",@F;
system("pdftk $_ cat output anth/ALTA/pdf/ALTA00.pdf");
system("cat anth/ALTA/bib/* > anth/ALTA/anth.bib");
system("cp Proceedings.pdf anth/ALTA/anth.pdf");

for($i=0;$i<=$#Pos;$i++) {
    ($a,$b) = split(/\-/,$Pos[$i]);
    $a = $a + $fm;
    # PC 23.11.2012
    # $b = $a - 1 + $b;
    $b = $b + $fm;
    my $j = 1 + $i;
    if ($a == $b) {
	$a = sprintf("%04d", $a);
	if ($j < 10) {
	    system("cp pdf_pages/pg_$a.pdf anth/ALTA/pdf/ALTA0$j.pdf");
	}
	else {
	    system("cp pdf_pages/pg_$a.pdf anth/ALTA/pdf/ALTA$j.pdf");
	}
    }
    else {
	undef @F;
	for($h=$a;$h<=$b;$h++) {
	    $_ = sprintf("%04d", $h);
	    push @F,"pdf_pages/pg\_$_\.pdf";
	}
	$_ = join " ",@F;

	if ($j < 10) {
	    system("pdftk $_ cat output anth/ALTA/pdf/ALTA0$j.pdf");
	}
	else {
	    system("pdftk $_ cat output anth/ALTA/pdf/ALTA$j.pdf");
	}
    }
}
