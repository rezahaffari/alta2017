#!/bin/bash

for f in final/*.pdf; 
do
    num=`basename $f | sed 's/ALTA_2016_paper_//; s/.pdf//'`
    echo -n "$num	"; 
    #mdls -n kMDItemNumberOfPages "$f" | cut -c24-
    pdftk "$f" dump_data | grep NumberOfPages | sed 's/^.*: //'
done 
