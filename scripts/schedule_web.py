import sys, re
from datetime import datetime
from time import strptime

print r'<table class="table"><tbody>'

with open('programme.tsv') as fin:
    for line in fin:
        line = line.strip()
        if line:
            line = line.replace(r'\&', '&amp;')
            parts = line.split('\t')

            if parts[0].lower() in ['monday', 'tuesday', 'wednesday']:
                day = parts[0]
                if parts[0].lower()  == 'monday': dom = 5
                elif parts[0].lower() == 'tuesday': dom = 6
                else: dom = 7

                print r'<tr><td colspan="3"><h4>%s %dth</h4></td></tr>' % (day, dom)
            else:
                typ = parts[1].split()[0].lower()

                print r'<tr>'
                if typ == 'session':
                    print r'	<td colspan="3">%s</td>' % parts[1]
                else:
                    print r'	<th scope="row">%s</th>' % parts[0]

                    if typ == 'invited':
                        print r'	<td>%s:</td><td>%s</td>' % (parts[1], parts[2])
                    elif typ in ['presentation', 'paper']:
                        print r'	<td>%s: %s</td><td><i>%s</i></td>' % (parts[1], parts[4], parts[5])
                    else:
                        print r'	<td colspan="2">%s</td>' % parts[1]
                print r'</tr>'

print r'</tbody></table>'
