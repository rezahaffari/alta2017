#!/usr/bin/perl -w

#Makes an HTML index page and bib files

use strict;

use vars qw (@Pos $i $h @F $a $b $t $au $fm);

use File::Copy qw(copy);

#Create bib for book

mkdir "U16";
copy "antho.css", "U16/antho.css";

#the size of the front matter in pages
$fm = 9;


open(O,">U16/U16-1000.bib")||die;
print O "\@Book\{ALTA2016\:2016,\n";
print O  "editor    = \{Trevor Cohn\},\n";
print O "title \= \{Proceedings of the Australasian Language Technology Association Workshop 2016\}\,\n";
print O "month     \= \{December\}\,\n";
print O "year      \= \{2016\}\,\n";
print O "address   \= \{Melbourne\, Australia\},\n";
print O "url       \= \{http://www.aclweb.org/anthology/U16-1.pdf}\n}\n";
close(O);

open(H,">U16/index.html")||die;
print H << "END";
<html><head>
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ACL Anthology &raquo; U15</title><link rel="stylesheet" type="text/css" href="antho.css" />
</head>

<body>
<hr>
<div id="content">
<a name="1000"></a><h1>Proceedings of the Australasian Language Technology Association Workshop 2016</h1>
<p><a href="U16-1.pdf">U16-1</a> [<a href="U16-1.bib">bib</a>]: <b>Entire volume</b>
<p><a href=U16-1000.pdf>U16-1000</a>: <i>Front Matter</i>
END


system("cat U16/U16-1000.bib > U16/U16-1.bib");

undef @Pos;
open(I,"Proceedings.toc_aux")||die;
$i = 1001;

while(<I>) {
    chomp;

    /\\it (.*?) ?\} \\\\ ?(.*?) ?\}\{(\d+)\}\{(\d+)\}$/;
    $t = $1; $au = $2; $a = $3; $b = $4;
    push @Pos,"$a\-$b";
    open(O,">U16/U16-$i.bib")||die;
    $_ = $au;
    s / and /:/g;
    s/:[^:]* /:/g;
    s/^.*? //g; 
    s /[ \,\"\\]//g;
    print O "\@InProceedings\{$_:2016:ALTA2016\,\n";
    print O "author \= \{$au\}\,\n";
    print O "title \= \{$t\}\,\n";
    print O "booktitle \= \{Proceedings of the Australasian Language Technology Association Workshop 2016\}\,\n";
    print O "month     \= \{December\}\,\n";
    print O "year      \= \{2016\}\,\n";
    print O "address   \= \{Melbourne\, Australia\}\,\n";
    # PC 23.11.2012
    # $_ = $a + $b - 1;
    $_ = $b;
    print O "pages \= \{$a\-\-$_\}\,\n";
    my $e = $_;
    $_ = 1000 + $i;
    print O "url \= \{http://www.aclweb.org/anthology/U16\-$i\}\n";
    print O "\}\n";
    close(O);
    system("cat U16/U16-$i.bib >> U16/U16-1.bib");


    ##Print index file
    print H "<p><a href=\"U16-$i.pdf\">U16-$i</a> ";
    print H "[<a href=\"U16-$i.bib\">bib</a>]: ";
    print H "<b>$au</b>";
    print H "<br><i>$t</i>";
    #print "<tr class=\"bg1\">\n";
    #print "<td valign=\"top\"><a href=\"U16/U16-$i.pdf\">pdf<\/a><\/td>\n";
    #print "<td valign=\"top\"><a href=\"U16/U16-$i.bib\">bib<\/a><\/td>\n";
    #print "<td valign=\"top\"><i>$t</i><br><b>$au<\/b><\/td>\n";
    #print "<td valign=\"top\"><a name=\"49\">pp.&nbsp;$a\&\#8211\;$e\<\/a><\/td>\n";
    #print "<\/tr>\n";
    $i++;
}
close(I);

print H "</div>\n</body></html>\n";
close(H);

#copy preface
undef @F;
for($h=1;$h<=$fm;$h++) {
    $_ = sprintf("%04d", $h);
    push @F,"pdf_pages/pg\_$_\.pdf";
}
$_ = join " ",@F;
system("pdftk $_ cat output U16/U16-1000.pdf");
#system("cat U16/* > anth.bib");
#system("cp anth.bib U16/U16-1.bib");
system("cp Proceedings.pdf anth.pdf");
system("cp Proceedings.pdf U16/U16-1.pdf");


for($i=0;$i<=$#Pos;$i++) {
    ($a,$b) = split(/\-/,$Pos[$i]);
    $a = $a + $fm;
    # PC 23.11.2012
    # $b = $a - 1 + $b;
    $b = $b + $fm;
    my $j = 1001 + $i;
    if ($a == $b) {
	$a = sprintf("%04d", $a);
	system("cp pdf_pages/pg_$a.pdf U16/U16-$j.pdf");
    }
    else {
	undef @F;
	for($h=$a;$h<=$b;$h++) {
	    $_ = sprintf("%04d", $h);
	    push @F,"pdf_pages/pg\_$_\.pdf";
	}
	$_ = join " ",@F;

	system("pdftk $_ cat output U16/U16-$j.pdf");
    }
}



