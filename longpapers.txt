27	Aili Shen, Jianzhong Qi and Timothy Baldwin	A Hybrid Model for Quality Assessment of Wikipedia Articles
29	Xiang Dai, Sarvnaz Karimi and Cecile Paris	Medication and Adverse Event Extraction from Noisy Text
18	Fei Liu, Trevor Cohn and Timothy Baldwin	Improving End-to-End Memory Networks with Unified Weight Tying
22	Shivashankar Subramanian, Trevor Cohn, Timothy Baldwin and Julian Brooke	Joint Sentence-Document Model for Manifesto Text Analysis
12	Oliver Adams, Trevor Cohn, Graham Neubig and Alexis Michaud	 Phonemic Transcription of Low-Resource Tonal Languages
20	Leonardo Dos Santos Pinheiro and Mark Dras	Stock Market Prediction with Deep Learning: A Character-based Neural Language Model for Event-based Trading
28	Katharine Cheng, Timothy Baldwin and Karin Verspoor	Automatic Negation and Speculation Detection in Veterinary Clinical Text
17	Shunichi Ishihara	A Comparative Study of Two Statistical Modelling Approaches for Estimating Multivariate Likelihood Ratios in Forensic Voice Comparison
7	Ming Liu, Gholamreza Haffari, Wray Buntine and Michelle Ananda-Rajah	Leveraging linguistic resources for improving neural text classification
14	Maria Myunghee Kim	Incremental Knowledge Acquisition Approach for Information Extraction on both Semi-structured and Unstructured Text from the Open Domain Web
