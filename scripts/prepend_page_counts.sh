#!/bin/bash

if [ ! -f "$1" ]
then
    echo 'Usage: <input file> <output file>'
    exit 1;
fi

cut -f 1 $1 | sort -n | sed 's/^/^/; s/$/	/' > tmp.ps
grep -f tmp.ps page_counts.txt | sort -n > tmp.pc
cut -f 1 tmp.pc > tmp.p
cut -f 1 $1 > tmp.tp

if cmp tmp.p tmp.tp
then
    echo "Ok"
else
    echo "Error: paper ids do not align"
    exit 1;
fi

cut -f 2 tmp.pc > tmp.c
paste tmp.c $1 > tmp.paste
mv tmp.paste $2

rm tmp.ps tmp.pc tmp.p tmp.c tmp.tp
