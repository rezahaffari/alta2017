import sys, re

day = sys.argv[1]
rest = sys.argv[2]

where = 'Monash Caulfield Campus, B214'

# takes the google sheet .tsv export format, and renders into latex

start = False

for line in open('programme.tsv'):

    ls = line.strip()
    if start and not ls:
        break

    if ls == day and not start:
        last = ''
        start = True
        print r"\vfill"
        print r"\begin{small}"
        print r"\noindent"
        print r"\begin{tabular}{p{.15\linewidth} p{.85\linewidth}}"
        print r"\multicolumn{2}{l}{\textbf{\large %s, %s}} \\[4pt]" % (day, rest)
        print r"\hline"
        pass
    elif start:
        parts = ls.split('\t')
        time = parts[0]

        if last:
            print last.format(time)

        typ = parts[1].strip()
        if typ:
            w1 = typ.split()[0]
            if w1 == 'Session':
                author = title = None
                print r'\multicolumn{2}{l}{%s (%s)} \\[4pt]' % (typ, where)
                last = ''
                continue

        author = title = None
        if w1 == 'Invited':
            title = parts[2]
            last = r'%s--{} & %s \\ & \emph{{%s}} \\[4pt]' % (time, parts[1], title)
        elif typ in ['Paper', 'Presentation']:
            author = parts[4]
            title = parts[5]
            if title[-1] == '.': del title[-1]
            last = r'%s--{} & %s: %s \\ & \emph{{%s}} \\[4pt]' % (time, typ, author, title)
        elif typ in ['Morning tea', 'Afternoon tea', 'Lunch']:
            print r'\hline ~\\[-11pt]'
            last = r'%s--{} & %s \\ \hline ~\\[-11pt]' % (time, typ)
        else:
            last = r'%s--{} & %s \\[4pt]' % (time, typ)
            #if w1 == "Shared":
                #last = last + '\n' + r'\input{{tex/sharedtask-tlist.tex}} \\[4pt]'
            if w1 == "Short-paper":
                last = last + '\n' + r'\input{{tex/shortpapers-tlist.tex}} \\[-6pt]'

if start:
    print r"\hline"
    print r"\end{tabular}"
    print r"\end{small}"
