#!/usr/bin/env python
"""
Convert db file to tab-separated format expected by proc.pl.

Call as, e.g.:
    python convert.py db
"""
import csv

ID = 'P'
TITLE = 'T'
AUTHOR = 'A'
PATH = 'F'
LENGTH = 'L'

class Data(object):
    def __init__(self, data=[]):
        self.data = data

    def db_read(self, db):
        record = {}
        for k, v in self.db_iter_lines(db):
            if k == '':
                if record:
                    self.data.append(record)
                record = {}
            elif k == AUTHOR:
                record.setdefault(AUTHOR, []).append(v)
            else:
                record[k] = v
                
    def db_iter_lines(self, db):
        for line in open(db):
            k, v = '', ''
            if line.strip() != '':
                k, v = line.strip().split(':', 1)
            yield k, v.strip()

    def ts_write(self):
        w_papers = csv.writer(open('papers.txt', 'w'), delimiter='\t')
        w_order = open('order.txt', 'w')
        for record in self.data:
            w_papers.writerow(self.ts_fmt(record))
            print >>w_order, record[ID]

    def ts_fmt(self, record):
        author = self.ts_author(record)
        return [record[LENGTH], record[ID], author, record[TITLE]]

    def ts_author(self, record):
        author = ''
        for i, a in enumerate(record[AUTHOR]):
            last, first = a.split(',')
            a = '{} {}'.format(first.strip(), last.strip())
            if i == 0:
                author = a
            elif i == len(record[AUTHOR])-1:
                author = '{} and {}'.format(author, a)
            else:
                author = '{}, {}'.format(author, a)
        return author

if __name__ == '__main__':
    import argparse
    p = argparse.ArgumentParser(description='Convert db to tsv.')
    p.add_argument('db', help='db file')
    args = p.parse_args()
    d = Data()
    d.db_read(args.db)
    d.ts_write()
