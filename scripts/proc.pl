#!/usr/bin/perl -w

# In the version of this script from David papers_fname and
# order_fname were hardcoded to "papers.txt" and "order.txt",
# respectively.


use strict;

use vars qw (@F %H $a $t $t2 $p $paux $papers_fname $order_fname $year);
$papers_fname = $ARGV[0];
$order_fname = $ARGV[1];

#SSW2014: adding year variable for easier updating
$year = 2016;

undef %H;
open(I,$papers_fname)||die;
while(<I>) {
    chomp;
	#This has to be tab delimited otherwise name/title tokens will be considered separate fields
    @F = split(/\t/,$_);
    $H{$F[1]}{a} = $F[2];
    $H{$F[1]}{t} = $F[3];
    $H{$F[1]}{p} = $F[0];
}
close(I);

open(I,$order_fname)||die;
while(<I>) {
    chomp;
    @F = split(/\s+/,$_);

    $a = $H{$F[0]}{a};
    $t = $H{$F[0]}{t};
    $p = $H{$F[0]}{p};
    $paux = $p - 1;

    if (substr($t, -1) eq '?' || substr($t, -1) eq '.') {
	$t2 = $t
    }
    else {
    	$t2 = $t . ".";
    }

    print "\\addcontentsline\{toc\}\{section\}\{\{\\it $t \} \\\\ $a \}\n";
    if ($p == 1) {
    print "\\includepdf\[pages\=1\-1\,pagecommand\=\{\\thispagestyle\{fancy\}\{\\fancyfoot\[c\]\{\}\\fancyfoot\[l\]\{\\footnotesize $a\. $year\. $t2 In \{\\it Proceedings of Australasian Language Technology Association Workshop\}\, page \\thepage.\}\\fancyhead\{\}\}\}\]\{final/ALTA_$year\_paper\_$F[0]\.pdf\}\n";
    } 
    else {
    print "\\includepdf\[pages\=1\-1\,pagecommand\=\{\\thispagestyle\{fancy\}\{\\fancyfoot\[c\]\{\}\\fancyfoot\[l\]\{\\footnotesize $a\. $year\. $t2 In \{\\it Proceedings of Australasian Language Technology Association Workshop\}\, pages \\thepage \$\-\$\\addtocounter\{page\}\{$paux\}\\thepage\{\}\\addtocounter\{page\}\{\-$paux\}.\}\\fancyhead\{\}\}\}\]\{final/ALTA_$year\_paper\_$F[0]\.pdf\}\n";
    }
    if ($p >= 2) {
	print "\\includepdf\[pages\=2\-$p\,pagecommand\=\{\\thispagestyle\{plain\}\}\]\{final/ALTA_$year\_paper\_$F[0]\.pdf\}\n\n";
    }
}
close(I);
